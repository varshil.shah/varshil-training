/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableOpacity,
} from "react-native";


import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from "react-native/Libraries/NewAppScreen";

const Section = () => {
  const isDarkMode = useColorScheme() === "dark";
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}
      >
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}
      >
        {children}
      </Text>
    </View>
  );
};

const App =  ()  => {
  const isDarkMode = useColorScheme() === "dark";

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={{flex:1}}>
      <ScrollView>
        <View style={styles.header}>
          <Image source={require("./assets/Logo.png")} />
          <Image source={require("./assets/Vector-42.png")} />
        </View>

        <View
          style={styles.login}
        >
          <Image style={styles.image} source={require("./assets/Login.png")} />
        </View>

        <View>
          <Text style={styles.txt}>See all your bookings</Text>
          <Text style={styles.txt1}>
            Log in, keep track of all your requests, accept {"\n"} new requests
            and easily reach to the {"\n"} customer
          </Text>
        </View>
        <View style={{

        }}>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.text}>Skip</Text>
          </TouchableOpacity>
        </View>
        
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: "600",
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: "400",
  },
  login: {
    
    justifyContent: "center",
    alignItems: "center",
  },
  
  txt: {
    display:'flex',
    textAlign: "center",
    paddingTop: 20,
    color: "#3D5EAB",
    fontWeight: "700",
    fontSize: 20,
    lineHeight: 24,
    fontSize: 22,
  },
  txt1: {
   
    textAlign: "center",
    paddingTop: 10,
    color: "#3D5EAB",
    fontWeight: "400",
    lineHeight: 16,
    fontSize: 15,
  },
  header: {
    flex:1,
    flexDirection: "row",
    justifyContent: "space-between",

  },
  image: {
    // marginLeft: '18%'
  },
  button: {
    marginTop:"20%",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: "2%",
    paddingHorizontal: "3%",
    borderRadius: 50,
    elevation: 3,
    backgroundColor: "#3D5EAB",
    width: 100,
    alignSelf: "center",
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    letterSpacing: 0.25,
    color: "white",
  },
 
});

export default App;
